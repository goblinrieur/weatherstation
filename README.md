# pico-weather-station

last changes : Mon 29 Mar 2021 02:23:25 PM CEST

last tag : v0.4

MPPT (pro version _not mine_) : purchased PCBWAY [Order ID: G680870 Delivery Address Order time & date : 2021/3/25 1:48:33)](PCBWAY.com)  

[PCB is in production](https://gitlab.com/goblinrieur/weatherstation/-/blob/MPPT/MPPT/Readme.md)


Tue 13 Apr 2021 02:49:35 PM CEST : PCB shipped 

# GOAL

will replace own-arduino one & update DHT11 with DHT22

https://gitlab.com/goblinrieur/diy_humidity_sensor/-/tree/master

update powering => MPPT + solar panel & battery (both)

- battery might be a 3.7v 18650 or a 7.4v pack _with regulation where needed_
- may use at term a OLED display instead of the Liquid Crystal one.
- may add a pressure measurement sensor.
- may also add a rain captor.
- base temperature & humidity sensor on one-wire version of DHT22 

why not also a Lora or WiFi or Bluetooth module to send me the reports.

# images

## Logical view

![new diagram](./Basics.png)

## Idea for testing

![first schema](./basic_test_to_do.png)

## Old weather station

![old one](https://gitlab.com/goblinrieur/diy_humidity_sensor/-/raw/master/FULL.png)

# old test of µPython

https://gitlab.com/goblinrieur/poc_linux_esp32_connection_micropython

# steps to work on it

## done last update : Wed 31 Mar 2021 03:18:17 PM CEST

- Draw diagram _(do not make it too much details yet)_
- List components
- Test how to use each one 
- [how to setup raspberry pi pico with micro python.](https://youtu.be/yzsEr2QCGPw)
- Test all solution wit breadboard
- Choose each components specs
- Choose components & purchase them 
    -   0.96" OLED IIC Yellow-Blue Display
    -   BMP180 atmospheric pressure
    -   DHT22 humidity & temperature
    -   BH1750FVI light captor
    -   FC37 rain captor
    -   CCS811 air quality sensor
    -   put rp2-pico-20210205-unstable-v1.14-8-g1f800cac3.uf2 on pico (mounted)
    -   MQ75 gaze detector/sensor
- try given DHT22 driver see DHT22_TEST branch for details
- try given code for few components in branches
    - DHT22_TEST
    - devices_isolated_tests
- get PCB for MPPT with 18650 battery rescue
    - shipping from [PCBWAY](http://www.pcbway.com) PCB for [MPPT](shipping.png) 
- Write python code
- Test how to use each one 
- [how to setup raspberry pi pico with micro python.](https://youtu.be/yzsEr2QCGPw)
- Test all solution wit breadboard
- Design a new functional diagram _this one must be detailed_
- Design an components connections diagram 

sample isolated test for each device.

each one have a import file or more; here I just show the test code itself and nothing else.

```
# testing oled screen
import machine 
import ssd1306 
import framebuf

WIDTH = 128
HIGHT = 64

#i2c= I2C(scl = Pin(17), sda = Pin(16), machine.I2C(0))

oled = ssd1306.SSD1306_I2C(WIDTH, HIGHT,machine.I2C(0))

oled.fill(0)

oled.text("hello world",0 ,0)
oled.text("this is me",50 ,50)
oled.show()
```

```
# sensor test
from machine import Pin
from DHT22 import DHT22
import utime


dht_sensor=DHT22.DHT22(Pin(15,Pin.IN,Pin.PULL_UP))
while True:
    T,H = dht_sensor.read()
    print("{}'C  {}%".format(T,H))
    #if T is None:
    #    print(" sensor error")
    #else:
    #    print("{}'C  {}%".format(T,H))
    #DHT22 not responsive if delay to short
    utime.sleep_ms(2000)
```

```
# isolated test for bh1750
import machine
from machine import I2C,Pin
from bh1750 import BH1750

# init eps8266 i2c
#scl = machine.Pin(5)
#sda = machine.Pin(4)
#i2c = machine.I2C(scl,sda)
i2c = I2C(0,scl=Pin(5), sda=Pin(4))
s = BH1750(i2c)

while True:
    s.luminance(BH1750.ONCE_HIRES_1)
```

```
# isolated test of bmp180 
from bmp180 import BMP180
from time import sleep
from machine import I2C, Pin                        # create an I2C bus object accordingly to the port you are using
# bus = I2C(1, baudrate=100000)           # on pyboard
bus = I2C(0)
#bus =  I2C(scl=Pin(17), sda=Pin(16), freq=100000)
#bus =  I2C(scl=Pin(4), sda=Pin(5), freq=100000)# on esp8266
bmp180 = BMP180(bus)
bmp180.oversample_sett = 2
bmp180.baseline = 101325

while True:
    tempc = bmp180.temperature
    Pressure = bmp180.pressure
    tempf = tempc*9/5+32
    p = bmp180.pressure
    altitude = bmp180.altitude
    # print(temp,(temp*9/5+32:.F2), p, altitude)
    # print("Temp: {0:.2f}C TempF: {1:.2f} Pressure: {2:.2f}  Altitude {3}".format(tempc,tempf,p*0.0002953,altitude))
    print('Temp: {0:.2f}'.format(tempc))
    print('pressure: {0:.2f}'.format(Pressure))
    sleep(5)
```

workdir : 
```
├── devices_isolated_tests
│   ├── bh1750
│   ├── bmp180
│   ├── dht
│   └── oled
├── dht22_tests
├── MPPT
│   ├── Fabrication_data
│   │   ├── 3D step
│   │   ├── BOM
│   │   ├── Gerber
│   │   ├── Image
│   │   ├── NC Drill
│   │   ├── PDF
│   │   └── Pick and place
│   └── PCBWAY
└── UF2
```


## TO-DO 

- Design a motherboard for all needed connectors _(with kicad or any other)_
- Test a motherboard on test board 
- Get Ideas of code from arduino/adafruit etc... & adapt them to our needs

- Document python code
- Purchase final motherboard design gerber & drill files _(to JLCPCB or PCBWAY or any other)_
- From its dimensions, select a case for the solution _(waterproof might be a good idea)_
- Receive it & solder all needed connectors & components
- insert this all in the case

