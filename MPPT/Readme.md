here is a bought schematic from a professional 

from [fiverr.com](fiverr.com) [smart_electro](https://www.fiverr.com/smart_electro)

I would prefer to work on a personal simplified version inspired by his work on it.

see file : [Fabrication_data.rar](Fabrication_data.rar)

To purchase on a PCB provider, I need names of layers :

- Top layer
- Ground plane
- Power plane
- Bottom

Mon 29 Mar 2021 02:18:56 PM CEST

[PCB in production](PCBWAY/PROD.png)
