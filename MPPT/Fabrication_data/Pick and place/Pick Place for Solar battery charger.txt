Altium Designer Pick and Place Locations
D:\Data\PCB Designs\Fiverr data\Goblinrieur\Solar battery charger\Solar battery charger\Project Outputs for Solar battery charger\Pick Place for Solar battery charger.txt

========================================================================================================================
File Design Information:

Date:       13/03/21
Time:       13:55
Revision:   Not in VersionControl
Variant:    No variations
Units used: mil

Designator Comment      Layer       Footprint     Center-X(mil) Center-Y(mil) Rotation Description                                                
C1         4.7uF/0805   TopLayer    RES_0805/2012 625.787mil    815.795mil    0        CAP CER 4.7UF 50V X7S 0805                                 
C2         4.7uF/0805   TopLayer    RES_0805/2012 625.787mil    735.795mil    0        CAP CER 4.7UF 50V X7S 0805                                 
C3         100UF/50V    TopLayer    100uf         388.787mil    856.795mil    90       CAP ALUM 100UF 20% 50V SMD                                 
C4         10uF/50V     BottomLayer 1206/3216     388.787mil    841.795mil    270      CAP CER 10UF 50V X5R 1206                                  
C5         1uF/0603     TopLayer    CAPC1608L     965.787mil    797.796mil    360      1uF, 50v, NP                                               
C6         100uf/10V    TopLayer    1206/3216     1505.787mil   852.795mil    90       CAP CER 100UF 10V X5R 1206                                 
C7         10uF/0603    BottomLayer CAP_0603/1608 1390.787mil   787.795mil    270      Capacitor                                                  
C8         .022UF/0603  BottomLayer CAPC1608L     1390.788mil   592.794mil    90       CAP CER 0.022UF 16V X7R 0603                               
C9         .022UF/0603  BottomLayer CAPC1608L     1460.788mil   592.794mil    90       CAP CER 0.022UF 16V X7R 0603                               
C10        10uF/0603    TopLayer    CAP_0603/1608 579.000mil    285.000mil    180      Capacitor                                                  
C11        10uF/0603    TopLayer    CAP_0603/1608 579.000mil    225.003mil    180      Capacitor                                                  
C12        1uF/0603     TopLayer    CAPC1608L     576.757mil    159.002mil    180      1uF, 50v, NP                                               
C13        1uF/0603     TopLayer    CAPC1608L     1325.999mil   296.999mil    90       1uF, 50v, NP                                               
C14        22uF/0805    TopLayer    RESC2012L     1254.000mil   297.001mil    270      Capacitor                                                  
C15        22uF/0805    TopLayer    RESC2012L     1173.000mil   297.001mil    270      Capacitor                                                  
C16        22uF/0805    TopLayer    RESC2012L     1086.000mil   298.496mil    270      Capacitor                                                  
C17        0.1uF/0603   TopLayer    CAPC1608L     909.174mil    106.363mil    270      0.1uF, 50v, NP                                             
C18        10pF         TopLayer    CAP_0603/1608 930.000mil    312.000mil    360      CAP CER 10PF 50V X8G 0603                                  
D1         CMSH3-40MA   TopLayer    SMA           303.000mil    504.000mil    270      DIODE SCHOTTKY 40V 3A SMA                                  
D2         CMSH3-40MA   TopLayer    SMA           459.000mil    504.000mil    90       DIODE SCHOTTKY 40V 3A SMA                                  
D3         CMSH3-40MA   BottomLayer SMA           1260.787mil   702.795mil    270      DIODE SCHOTTKY 40V 3A SMA                                  
D4         CMPSH1-4     BottomLayer SOT23-F       1085.787mil   677.795mil    90       DIODE SCHOTTKY 40V 1.75A SOT23F                            
D5         CMSH3-40MA   BottomLayer SMA           381.000mil    537.000mil    180      DIODE SCHOTTKY 40V 3A SMA                                  
D6         CMSH3-40MA   BottomLayer SMA           381.000mil    378.000mil    180      DIODE SCHOTTKY 40V 3A SMA                                  
J1         Header       TopLayer    2.0mm         72.000mil     596.630mil    270      CONN HEADER VERT 2POS 2MM                                  
J2         Header       TopLayer    2.0mm         705.787mil    1032.795mil   180      CONN HEADER VERT 2POS 2MM                                  
J3         Header       TopLayer    2.0mm         1545.787mil   592.795mil    90       CONN HEADER VERT 2POS 2MM                                  
J4         Header       TopLayer    2.0mm         141.787mil    67.795mil     0        CONN HEADER VERT 2POS 2MM                                  
J5         Header       TopLayer    2.0mm         395.630mil    69.000mil     0        CONN HEADER VERT 2POS 2MM                                  
J6         Header       TopLayer    2.0mm         1353.173mil   76.362mil     0        CONN HEADER VERT 2POS 2MM                                  
J7         Header       TopLayer    2.0mm         1104.787mil   74.795mil     0        CONN HEADER VERT 2POS 2MM                                  
L1         10UH         TopLayer    Inductor      1205.787mil   712.795mil    270      FIXED IND 10UH 4A 71.2 MOHM SMD                            
L2         1.0uH        TopLayer    1.0uH         756.000mil    378.000mil    180      Fixed Inductors 1Uh Shld 11A 11.9mOhms AECQ2               
LD1        RED LED      TopLayer    LED_1608L     718.001mil    517.000mil    360      RED LED SMT                                                
M1         IRF8714TRPBF BottomLayer SOIC-8        711.000mil    735.000mil    270      MOSFET N-CH 30V 14A 8SO                                    
M2         IRF9335TRPBF BottomLayer SOIC-8        711.000mil    432.000mil    270      MOSFET P-CH 30V 5.4A 8-SOIC                                
R1         536K         TopLayer    RESC1608L     775.787mil    817.795mil    180      RES 536K OHM 1% 1/10W 0603                                 
R2         100K/0603    TopLayer    RESC1608L     775.787mil    752.795mil    0        100KR 0.1% 1/10W 0603                                      
R3         1M           TopLayer    CAPC1005L     785.787mil    697.795mil    360      RES SMD 1M OHM 1% 1/10W 0603                               
R4         5.1K         TopLayer    RESC1608L     775.787mil    582.795mil    180      RES SMD 5.1K OHM 1% 1/10W 0603                             
R5         5.1K         TopLayer    RESC1608L     775.787mil    642.795mil    180      RES SMD 5.1K OHM 1% 1/10W 0603                             
R6         0.05R        TopLayer    1206/3216     1150.787mil   457.795mil    0        RES 0.05 OHM 1% 1W 1206                                    
R7         542K         TopLayer    RESC1608L     1015.787mil   417.795mil    90       RES 542K OHM 0.5% 1/10W 0603                               
R8         0R/0603      TopLayer    RESC1608L     1015.787mil   532.795mil    90       0R                                                         
R9         0.1R/0603    TopLayer    RESC1608L     1390.787mil   842.795mil    90       RES 0.1 OHM 1% 1/5W 0603                                   
R10        1KR/0603     BottomLayer RESC1608L     177.000mil    264.000mil    360      1KR, 0.25w, 1%                                             
R11        1KR/0603     BottomLayer RESC1608L     897.000mil    828.000mil    360      1KR, 0.25w, 1%                                             
R12        459K         TopLayer    RESC1608L     950.788mil    532.795mil    270      RES 459K OHM 0.25% 1/10W 0603                              
R13        1KR/0603     BottomLayer RESC1608L     765.000mil    933.000mil    360      1KR, 0.25w, 1%                                             
R14        1KR/0603     BottomLayer RESC1608L     921.000mil    525.000mil    180      1KR, 0.25w, 1%                                             
R15        1KR/0603     BottomLayer RESC1608L     295.000mil    264.000mil    360      1KR, 0.25w, 1%                                             
R16        1KR/0603     BottomLayer RESC1608L     933.000mil    339.000mil    180      1KR, 0.25w, 1%                                             
R17        1KR/0603     TopLayer    RESC1608L     1422.000mil   270.000mil    360      1KR, 0.25w, 1%                                             
R18        1M           TopLayer    CAPC1005L     589.000mil    92.000mil     360      RES SMD 1M OHM 1% 1/10W 0603                               
R19        1M           TopLayer    CAPC1005L     920.433mil    189.000mil    360      RES SMD 1M OHM 1% 1/10W 0603                               
R20        1M           TopLayer    CAPC1005L     771.000mil    51.000mil     180      RES SMD 1M OHM 1% 1/10W 0603                               
R21        1M           TopLayer    CAPC1005L     588.567mil    51.003mil     360      RES SMD 1M OHM 1% 1/10W 0603                               
R22        110K         TopLayer    RESC1608L     932.244mil    243.000mil    180      0R                                                         
TR1        10K/NTC      TopLayer    CAP_0603/1608 885.787mil    532.795mil    90       SENSOR THERMISTOR NTC                                      
U1         LT3652EDD    TopLayer    DFN           970.787mil    687.795mil    0        IC BATT CHG MULTI-CHEM 12DFN                               
U2         TPS63060DSCT TopLayer    TPS63060DSCT  765.000mil    189.000mil    0        Switching Voltage Regulators 2.5-12Vin,2.25A Sw Crnt Limit 
